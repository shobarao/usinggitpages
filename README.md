# How I learnt to use gitpages

I am using this project to learn how to use gitlab, gitlabPages and Jekyll to create and publish a set of user documentation.

## To get started

1. I signed up with gitlab and created a new project
2. Cloned the repo on my local computer
3. Created and added a README file
4. Commited and pushed the file to the origin master

