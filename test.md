# Phoenix SDK
The Phoenix SDK is a design and development kit that helps you:
  - create apps based on open web standards
  - build and run the apps
  - test and verify the user interface and interaction

Phoenix provides a unified web application development platform where native services are built on top of chromium browser. It takes advantage of the native services running in chromium and connects to the internet for cloud apps functionality. Application development is a lot easier with sophisticated and simple to understand HTML5/JS code with unified set of APIs based on open web standards handled by the browser.

## Installing Phoenix SDK
Just download it from the link sent to your email; save and run it. You’re all set!

[![IMAGE ALT TEXT HERE](http://img.youtube.com/vi/Rykmwn0SMWU/0.jpg)](http://www.youtube.com/watch?v=Rykmwn0SMWU)

## Phoenix Developer Tools
The Phoenix Developer Tools are a set of infotainment app development and debugging tools. It consists of:
- Elements
- Infotainment
- Console
- Source
- Network
- Timeline
- Profiles
- Resources
- Security
- Audits
```
In this release, we will focus on the **Infotainment** panel.
```
### Accessing the Infotainment Panel
Once you've launched the Visteon Shell, right-click and select **Inspect Element**. This launches the Phoenix Developer Tools window, click on the **Infotainment** panel.
[![IMAGE ALT TEXT HERE](http://img.youtube.com/vi/YOUTUBE_VIDEO_ID_HERE/0.jpg)](http://www.youtube.com/watch?v=YOUTUBE_VIDEO_ID_HERE)

## Infotainment Panel
Use the **Infotainment** panel to iterate on the design and development of your app by adding the devices, loading the vehicle profiles and controllers. The **Infotainment** panel consists of:
- Device Manager - lists the devices and the details associated with the devices
- Vehicle Profiles - lists the vehicle profiles that can be downloaded
- API Reference - displays the API reference documentation

### Device Manager
Use the DeviceManager panel to add, remove, connect,disconnect and look-up device details. The device details include:
| Device details | Description |
| -------------- | ------------|
| No.            | Serial number associated with the device added. |
| Device Name    | States the name you entered while adding the device. |
| Device Type    | States the type selected while adding the device. |
| Device ID      | States the 64-bit unique ID associated with the device. |
| Connect/Disconnect | States the device state. |
| Remove         | States that the device can be removed. |

In this release of the SDK you can simulate the following conditions:
- add/remove device
- connect/disconnect device
```
The current version of the SDK supports inclusion of audio files only, while adding the device.
```
#### Adding a Device
To add a device:
1. Go to **Infotainment** panel
2. Click on ![Add icon located to the bottom right of the Infotainment panel](/images/add-icon.png)
